'use strict';

var $proxyName = 'http://pyramids-markup/';
var $hostName = 'pyramids-markup';

const gulp = require('gulp');
const sass = require('gulp-sass');
const prefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const minify = require('gulp-minify-css');
const rigger = require('gulp-rigger');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');
const newer = require('gulp-newer');
const browserSync = require('browser-sync').create();
const watch = require('gulp-watch');
const debug = require('gulp-debug');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const cache = require('gulp-cache');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const imageminJpegRecompress = require('imagemin-jpeg-recompress');
const svgSprite = require('gulp-svg-sprite');
const svgmin = require('gulp-svgmin');
const cheerio = require('gulp-cheerio');
const replace = require('gulp-replace');

gulp.task('browserSync', function() {
    browserSync.init({
        proxy: $proxyName,
        host: $hostName,
        open: 'external',
        port: 8080,
        notify: false,
        files: ['./**/*.php', './**/*.html', './**/*.css', './js/*.js']
    });
});

gulp.task('style:build', function() {
    return gulp.src('src/styles/application.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on('error', notify.onError(function(err) {
            return {
                title: 'Styles',
                message: err.message
            }
        }))
        .pipe(prefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(minify())
        .pipe(debug({title: 'styles result'}))
        .pipe(sourcemaps.write())
        .pipe(rename('style.css'))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('js:jQuery', function () {
  return gulp.src('src/js/jquery-3.1.0.min.js')
    .pipe(newer('js'))
    .pipe(gulp.dest('js'))
    .pipe(debug({
      title: 'js jquery result'
    }))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('jsCustom:build', function() {
    return gulp.src('src/js/custom/*')
        .pipe(newer('js/custom/app.min.js'))
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(debug({title: 'js custom result'}))
        .pipe(uglify())
        .on('error', notify.onError(function(err) {
            return {
                title: 'js',
                message: err
            }
        }))
        .pipe(sourcemaps.write())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js/custom'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('jsVendor:build', function() {
    return gulp.src(['src/js/vendor/*.js'])
        .pipe(newer('js/vendor/vendor.min.js'))
        .pipe(debug({title: 'js vendor result'}))
        .pipe(sourcemaps.init())
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .on('error', notify.onError(function(err) {
            return {
                title: 'js',
                message: err
            }
        }))
        //.pipe(sourcemaps.write())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js/vendor'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('js:build', gulp.series(
    'js:jQuery',
    'jsCustom:build',
    'jsVendor:build'
));

gulp.task('fonts', function() {
    return gulp.src('src/fonts/*')
        .pipe(newer('fonts'))
        .pipe(debug({title: 'fonts copying'}))
        .pipe(gulp.dest('fonts'))
});

gulp.task('svg:sprite', function () {
	return gulp.src('src/svg-sprite/*.svg')
    .pipe(svgmin({
        js2svg: {
            pretty: true
        }
    }))
    /*.pipe(cheerio({
        run: function ($) {
            $('[fill]').removeAttr('fill');
            $('[stroke]').removeAttr('stroke');
            $('[style]').removeAttr('style');
            $('style').remove();
        },
        parserOptions: {xmlMode: true}
    }))
    .pipe(replace('&gt;', '>'))*/
    .pipe(svgSprite({
        mode: {
           symbol: {
           dest: 'svg-sprite',
           example: true,
           sprite: 'sprite.svg'
         },
        }
    }))
    .pipe(gulp.dest('images'));
});


gulp.task('clean', function() {
   return del(['style.css', 'js/vendor', 'js/custom', 'fonts', 'images/svg-sprite'])
});

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel(
    'style:build',
    'js:build',
    'fonts',
    'svg:sprite'
)));

gulp.task('watch', function() {
    gulp.watch('src/styles/**/*.*', gulp.series('style:build'));
    gulp.watch('src/js/**/*.*', gulp.series('js:build'));
    gulp.watch('src/fonts/*', gulp.series('fonts'));
    gulp.watch('src/svg-sprite/*', gulp.series('svg:sprite'));
});

gulp.task('default', gulp.series('build', gulp.parallel('watch', 'browserSync')));

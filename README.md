
## Task
`npm i` - setup project

`gulp default` - run project

## Structure
/                               - root
├── fonts                       - **generated** folder
├── js                          - **generated** folder
├── images                      - images - **copy to wp theme**
├── blocks                      - project blocks
├── src                         - sources of template - **copy to wp theme** with all contents
├──  ├── fonts                  - source folder
├──  ├── js                     - source folder
├──  ├── styles                 -  sass - source folder

├── style.css                   -  main style file - **generated** file
├── package.json                -  node.js dependencies - **copy to wp theme**
├── gulpfile.js                 -  gulp config - **copy to wp theme**
└── README.md                   -  readme - **copy to wp theme**

## Notes
1. To copy gulp features to wp theme:
    - in **gulpfile.js** change variables to your wp local domain:
        `var $proxyName = 'http://local-domain/';`
        `var $hostName = 'local-domain';`
    - copy only files and folders that are marked as **'copy to wp theme'**;
    - add to your **.gitignore** file in root wp folder:
        `wp-content/themes/your-theme-name/node_modules/*`;
    - all generated files and folders are rewrited by gulp, all changes are automatic, don't change them